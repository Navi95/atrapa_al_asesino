﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Book : MonoBehaviour , IInteractable {

	public bool state = false;
	public char letter;
	public int pos;
	public LibreroPuzzle librero;
	private int act;
	private AudioSource audio;



	// Use this for initialization
	void Start () {
		GameObject[] letras = GameObject.FindGameObjectsWithTag("letra");
		Aleatoriedad random = GameObject.Find("Random").GetComponent<Aleatoriedad>();
		librero = GameObject.Find("LibreroPuzzle").GetComponent<LibreroPuzzle>();
		letter = random.letrasDesordenadas[pos];
		letras[pos].GetComponent<SpriteRenderer>().sprite =  Resources.Load<Sprite>("Letras/" + letter);
		audio = GameObject.Find("Libro").GetComponent<AudioSource>();	
		
	}
	
	// Update is called once per frame
	void Update () {
	}	

	public void Activate(){
	}

	public void Interact(DisplayImage currentDisplay)
    {
		
		if(!state){
			GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/librosalido");
			librero.solution.Add(letter);
			act = librero.solution.Count;
			librero.checkSolution();
			audio.Play();
		}else{
			GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/empty_item2");
			librero.solution.Remove(letter);

		}
		state = !state;
	}
	
}
