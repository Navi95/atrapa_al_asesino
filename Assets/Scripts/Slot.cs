﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IPointerClickHandler
{
    private GameObject inventory;
    public GameObject item;

    public enum property { usable, displayable, dragable, empty };
    public property ItemProperty { get; set; }

    private string displayImage;
    public string combinationItem { get; private set; }
    public DisplayImage currentDisplay;
    void Start()
    {
        inventory = GameObject.Find("Inventory");
        currentDisplay = GameObject.Find("displayImage").GetComponent<DisplayImage>();
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        inventory.GetComponent<Inventory>().previousSelectedSlot = inventory.GetComponent<Inventory>().currentSelectedSlot;
        inventory.GetComponent<Inventory>().currentSelectedSlot = this.gameObject;
        //Combine();
        if (ItemProperty == Slot.property.displayable) DisplayItem();
        if (ItemProperty == Slot.property.dragable) DisplayLego();
    }

    public void AssignProperty(int orderNumber, string displayImage, string combinationItem)
    {
        ItemProperty = (property)orderNumber;
        this.displayImage = displayImage;
        this.combinationItem = combinationItem;
    }

    public void DisplayItem()
    {
        inventory.GetComponent<Inventory>().itemDisplayer.SetActive(true);
        inventory.GetComponent<Inventory>().itemDisplayer.GetComponent<Image>().sprite =
            Resources.Load<Sprite>("Inventory Items/" + displayImage);
            if( displayImage== "0"){
                    inventory.GetComponent<Inventory>().itemDisplayer.GetComponent<Image>().color = Color.red;
                }else if  (displayImage =="1"){
                    inventory.GetComponent<Inventory>().itemDisplayer.GetComponent<Image>().color = Color.blue;
                    
                }else if  (displayImage=="2"){
                    inventory.GetComponent<Inventory>().itemDisplayer.GetComponent<Image>().color = Color.yellow;
                    
                }else if  (displayImage =="3"){

                    inventory.GetComponent<Inventory>().itemDisplayer.GetComponent<Image>().color = Color.green;
                }
    }

    public void DisplayLego()
    {
        if (currentDisplay.escenaActual == item.GetComponent<Lego>().wall)
        {
            GameObject caja = GameObject.Find("caja");
            Vector3 posicion = caja.transform.position;
            posicion.x += UnityEngine.Random.Range(0.5f, 3.6f);
            posicion.y += UnityEngine.Random.Range(0.3f, 0.7f);
            item.transform.SetParent(caja.transform);
            item.GetComponent<Lego>().transform.position = posicion;

            if(item.GetComponent<SpriteRenderer>().sprite.name == "lego boligrafo")
            {
                item.GetComponent<RectTransform>().localScale = new Vector3(12.71982f, 40.06581f, 89.83878f);
                Vector3 rotation = new Vector3(-49.429f, 46.572f, -86.79601f);
                item.GetComponent<RectTransform>().rotation = Quaternion.Euler(rotation);
            }
            if (item.GetComponent<SpriteRenderer>().sprite.name == "lego moneda")
            {
                item.GetComponent<RectTransform>().localScale = new Vector3(13.30629f, 39.48432f, 54.30357f);
                Vector3 rotation = new Vector3(-72.094f, 1.222f, -2.316f);
                item.GetComponent<RectTransform>().rotation = Quaternion.Euler(rotation);
            }

            item.SetActive(true);
            ClearSlot();
        }
        else
        {
            DisplayItem();
        }

    }

    void Combine()
    {
        if (inventory.GetComponent<Inventory>().previousSelectedSlot.GetComponent<Slot>().combinationItem
            == this.gameObject.GetComponent<Slot>().combinationItem && this.gameObject.GetComponent<Slot>().combinationItem != "")
        {
            Debug.Log("combine");
            GameObject combinedItem = Instantiate(Resources.Load<GameObject>("Combined Items/" + combinationItem));
            combinedItem.GetComponent<PickUpItem>().ItemPickUp();

            inventory.GetComponent<Inventory>().previousSelectedSlot.GetComponent<Slot>().ClearSlot();
            ClearSlot();
        }
    }

    public void ClearSlot()
    {
        inventory.GetComponent<Inventory>().items -= 1;
        var scrollbar = GameObject.Find("Scrollbar Horizontal");
        scrollbar.GetComponent<Scrollbar>().value -= 0.18f;

        ItemProperty = Slot.property.empty;
        displayImage = "";
        combinationItem = "";
        transform.GetChild(0).GetComponent<Image>().sprite =
            Resources.Load<Sprite>("Inventory Items/empty_item");
    }
}
