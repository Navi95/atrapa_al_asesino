﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Aleatoriedad : MonoBehaviour {
    string[] palabras = {"TIVAN", "NEVIN", "LEILA"};
    public char[] letras;
    public char[] letrasDesordenadas;
    public char[] numeros;
    public char[] numerosCajaFuerte;
    public int numeroAleatorio;
    public int numeroCajaFuerte;
    GameObject[] espaciosPapelesVacios;
    GameObject[] espaciosPistasVacias;
    Stack<GameObject> pilaEspacioPapeles;
    Stack<GameObject> pilaEspacioPistas;
    public GameObject[] listaPapeles;
    public GameObject[] listaPistas;

    public int GenerateRandomNo()
    {
        int r = Random.Range(1000, 9999);
        return r;
    }

    void Shuffle(GameObject[] texts)
    {
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < texts.Length; t++)
        {
            GameObject tmp = texts[t];
            int r = Random.Range(t, texts.Length);
            texts[t] = texts[r];
            texts[r] = tmp;
        }
    }

    void shuffleArray(char[] array){
        
        for (int i = 0; i < array.Length; i++)
        {
            char tmp = array[i];
            int r = Random.Range(i, array.Length);
            array[i] = array[r];
            array[r] = tmp;
        }
    }


    // Use this for initialization
    void Start () {
        if (SceneManager.GetActiveScene().name == "nivel1")
        {
            randominzarLibros();
            numeroAleatorio = GenerateRandomNo();
            numeros = numeroAleatorio.ToString().ToCharArray();
            pilaEspacioPapeles = new Stack<GameObject>();
            pilaEspacioPistas = new Stack<GameObject>();


            espaciosPapelesVacios = GameObject.FindGameObjectsWithTag("espacioPapeles");
            espaciosPistasVacias = GameObject.FindGameObjectsWithTag("espacioPistas");
            Shuffle(espaciosPapelesVacios);
            Shuffle(espaciosPistasVacias);
            foreach (GameObject value in espaciosPapelesVacios) { pilaEspacioPapeles.Push(value); }
            foreach (GameObject value in espaciosPistasVacias) { pilaEspacioPistas.Push(value); }

            Shuffle(listaPapeles);
            Shuffle(listaPistas);
            foreach (GameObject value in listaPistas)
            {
                print("pista");
                GameObject espacio = pilaEspacioPistas.Pop();
                GameObject instancia = Instantiate(value, espacio.transform.position, espacio.transform.rotation, espacio.transform.parent);
                instancia.transform.localScale = espacio.transform.localScale;
                //value.transform.parent = pilaEspacios.Pop().transform;
            }

            foreach (GameObject value in listaPapeles)
            {
                GameObject espacio = pilaEspacioPapeles.Pop();
                GameObject instancia = Instantiate(value, espacio.transform.position, espacio.transform.rotation, espacio.transform.parent);
                instancia.transform.localScale = espacio.transform.localScale;
                //value.transform.parent = pilaEspacios.Pop().transform;
            }


        }
        
        //-------------------------------------------------- nivel 2 -------------------------------------------------

        if (SceneManager.GetActiveScene().name == "nivel2")
        {
            numeroCajaFuerte = GenerateRandomNo();
            numeroAleatorio = GenerateRandomNo();
            GameObject[] espacioStickyVacios = GameObject.FindGameObjectsWithTag("espacioSticky");
            Shuffle(espacioStickyVacios);
            Stack<GameObject> pilaSticky = new Stack<GameObject>();
            
            foreach (GameObject value in espacioStickyVacios) { pilaSticky.Push(value); }
            

            char[] numeroarray = numeroAleatorio.ToString().ToCharArray();
            int c = 0;;
            foreach(char n in numeroarray){
                print(listaPapeles.Length);
                GameObject espacio = pilaSticky.Pop();
                GameObject instancia = Instantiate(listaPapeles[n-48], espacio.transform.position, espacio.transform.rotation, espacio.transform.parent);
                instancia.transform.localScale = espacio.transform.localScale;
                print(c);
                if(c == 0){
                    instancia.GetComponent<SpriteRenderer>().color = Color.red;
                }else if  (c ==1){
                    instancia.GetComponent<SpriteRenderer>().color = Color.blue;
                    
                }else if  (c ==2){
                    instancia.GetComponent<SpriteRenderer>().color = Color.yellow;
                    
                }else if  (c ==3){

                    instancia.GetComponent<SpriteRenderer>().color = Color.green;
                }
                c++;
                
            }


            pilaEspacioPistas = new Stack<GameObject>();
            espaciosPistasVacias = GameObject.FindGameObjectsWithTag("espacioPistas");
            Shuffle(espaciosPistasVacias);
            foreach (GameObject value in espaciosPistasVacias) { pilaEspacioPistas.Push(value); }

            foreach (GameObject value in listaPistas)
            {
                GameObject espacio = pilaEspacioPistas.Pop();
                GameObject instancia = Instantiate(value, espacio.transform.position, espacio.transform.rotation, espacio.transform.parent);
                instancia.transform.localScale = espacio.transform.localScale;

                //value.transform.parent = pilaEspacios.Pop().transform;
            }
    
            var cajita = GameObject.Find("cajita");
            cajita.GetComponent<Cajita>().legos = FindObjectsOfType<Lego>();

           
            
            numerosCajaFuerte = numeroCajaFuerte.ToString().ToCharArray();
            GameObject[] espaciosNumerosVacios   = GameObject.FindGameObjectsWithTag("espacioNumeros");
            foreach(GameObject numero in espaciosNumerosVacios)
            {
                numero.GetComponent<NumeroBox>().num = numerosCajaFuerte[numero.GetComponent<NumeroBox>().pos];
            }            
         
        }





    }

    void randominzarLibros(){
        print("entro :v ");
		int numWord = Random.Range(0, palabras.Length);
		string word = palabras[numWord];
        letras = new char[5];
        letrasDesordenadas = new char[5];
        print(word);
		letras = word.ToCharArray();
        for (int i = 0; i < letras.Length; i++)
        {
            letrasDesordenadas[i]=letras[i];
        }
        shuffleArray(letrasDesordenadas);
    }
	

  
   
}
