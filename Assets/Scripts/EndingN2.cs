﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EndingN2 : MonoBehaviour {

	int count = 0;
	private Image img;

	// Use this for initialization
	void Start () {
		img = GetComponent<Image>();

	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetMouseButtonDown(0)){
			print(count);
			switch (count)
			{
				case 0 :
					img.sprite = Resources.Load<Sprite>("Ending/DialogoMuerto3");
					count++;
					break;
				case 1:
					img.sprite = Resources.Load<Sprite>("Ending/arrestadaKeila");
					GameObject.Find("presa").GetComponent<AudioSource>().Play();
					count++;
					break;			
				default:
					break;
			}
		}
		
	}
}
