﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeWall : MonoBehaviour, IInteractable {

    public string WallName;
    public string tipo;


    void Awake()
    {
        
    }

    public void Interact(DisplayImage currentDisplay)
    {

        currentDisplay.escenaAnterior = currentDisplay.escenaActual;
        currentDisplay.pilaEscenas.Push(currentDisplay.escenaActual);
        currentDisplay.escenaActual = WallName;
        if(tipo == "puerta"){
            AudioSource sonido  = GameObject.Find("PuertaAbierta").GetComponent<AudioSource>();
            sonido.Play();
        }


    }

  

}