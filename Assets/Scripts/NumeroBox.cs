﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumeroBox : MonoBehaviour {
    public char num;
    public int pos;
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame

    void Update()
    {
       
        SpriteRenderer numPositionRenderer = GetComponent<SpriteRenderer>();
        numPositionRenderer.sprite = Resources.Load<Sprite>("Numeros/" + num.ToString());

    }
}
