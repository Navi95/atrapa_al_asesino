﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CajaFuerte : MonoBehaviour {
    public Queue<NumeroCajaFuerte> numerosPulsados;
    public Animator perillaAnimator , cajaAnimator;
    public bool resuelto = false;
    public bool abierto = false;
    public GameObject objetoBloqueado;
	// Use this for initialization
	void Start () {
        objetoBloqueado.SetActive(false);
        numerosPulsados = new Queue<NumeroCajaFuerte>();
	}
	
	// Update is called once per frame
	void Update () {
        if(numerosPulsados.Count >= 4)
        {
            verificarClave();
        }
        if (resuelto && !abierto)
        {

            perillaAnimator.SetBool("abierta", true);
            if (this.perillaAnimator.GetCurrentAnimatorStateInfo(0).IsName("perilla abierta"))
            {
                
                cajaAnimator.SetBool("abierta", true);
                if (this.cajaAnimator.GetCurrentAnimatorStateInfo(0).IsName("caja abierta"))
                {
                    gameObject.transform.GetChild(0).gameObject.SetActive(false);
                    abierto = true;
                    objetoBloqueado.SetActive(true);
                }

            }
        }
	}

    void verificarClave()
    {
        Aleatoriedad random = GameObject.Find("Random").GetComponent<Aleatoriedad>();
        string Password = random.numeroCajaFuerte.ToString();
        string numeroPulsado = "";
        while (numerosPulsados.Count > 0)
        {
            numeroPulsado += numerosPulsados.Dequeue().numero;
        }
        if(numeroPulsado == Password)
        {
            resuelto = true;
            numerosPulsados.Clear();
        }
        else
        {
            AudioSource sonido = GameObject.Find("Error").GetComponent<AudioSource>();
            sonido.Play();

            numerosPulsados.Clear();
        }
    }
}
