﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class Lego : MonoBehaviour  , IDragHandler, IDropHandler
{

    public string wall;
    //public GameObject caja;
    public string forma;
    public  string formaContenedora { get; set; }
    public void OnDrag(PointerEventData eventData)

    {
        Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(newPosition.x, newPosition.y, transform.position.z);
        
    }

    public void OnDrop(PointerEventData eventData)
    {
        var cajita = GameObject.Find("cajita");


        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePosition2D = new Vector2(mousePosition.x, mousePosition.y);
        for (int i = 0; i < cajita.GetComponent<Cajita>().legoBoxes.Length; i++)
        {
            if (cajita.GetComponent<Cajita>().legoBoxes[i].GetComponent<BoxCollider2D>().OverlapPoint(mousePosition2D)
                )
            {

                transform.position = cajita.GetComponent<Cajita>().legoBoxes[i].transform.position;
                //cajita.GetComponent<Cajita>().legoBoxes[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Inventory Items/" + GetComponent<SpriteRenderer>().sprite.name); 
               // transform.rotation = cajita.GetComponent<Cajita>().legoBoxes[i].transform.rotation;
               // transform.parent = cajita.GetComponent<Cajita>().legoBoxes[i].transform;
                formaContenedora = cajita.GetComponent<Cajita>().legoBoxes[i].forma;
                AudioSource sonido = GameObject.Find("Iman").GetComponent<AudioSource>();
                sonido.Play();
            }
        }

       
    }

    // Use this for initialization
    void Start () {
        formaContenedora = " ";
    }
	
	// Update is called once per frame
	void Update () {
	}
}
