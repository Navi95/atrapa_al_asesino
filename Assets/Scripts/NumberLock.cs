﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NumberLock : MonoBehaviour {

    string Password;

    public GameObject OpenLockerSprite;

    private GameObject displayImage;

    [HideInInspector]
    public Sprite[] numberSprites;
    [HideInInspector]
    public int[] currentIndividualIndex = { 0, 0, 0, 0 };

    private bool isOpen;

    void Start()
    {
        displayImage = GameObject.Find("displayImage");
        OpenLockerSprite.SetActive(false);
        isOpen = false;
        LoadAllNumberSprites();
    }

    void Update()
    {
        OpenLocker();
        LayerManager();
    }

    void LoadAllNumberSprites()
    {
        numberSprites = Resources.LoadAll<Sprite>("Sprites/numbers");
    }




    bool VerifyCorrectCode()
    {

        if (SceneManager.GetActiveScene().name == "nivel1")
        {
            Aleatoriedad random = GameObject.Find("Random").GetComponent<Aleatoriedad>();
            Password = random.numeroAleatorio.ToString();
            bool correct = true;

            for (int i = 0; i < 4; i++)
            {
                if (Password[i] != transform.GetChild(i).GetComponent<SpriteRenderer>().sprite.name.Substring(transform.GetChild(i).GetComponent<SpriteRenderer>().sprite.name.Length - 1)[0])
                {
                    correct = false;
                }
            }

            return correct;
        }
        if (SceneManager.GetActiveScene().name == "nivel2")
        {
            Aleatoriedad random = GameObject.Find("Random").GetComponent<Aleatoriedad>();
            Password = random.numeroAleatorio.ToString();
            bool correct = true;

            for (int i = 0; i < 4; i++)
            {
                if (Password[i] != transform.GetChild(i).GetComponent<SpriteRenderer>().sprite.name.Substring(transform.GetChild(i).GetComponent<SpriteRenderer>().sprite.name.Length - 1)[0])
                {
                    correct = false;
                }
            }

            return correct;
        }
        return false;

    }
	

    void OpenLocker()
    {
        if (isOpen) return;

        if (VerifyCorrectCode())
        {
            isOpen = true;
            OpenLockerSprite.SetActive(true);
            AudioSource sonido = GameObject.Find("Chapa").GetComponent<AudioSource>();
            sonido.Play();
            for (int i = 0; i < 4; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }

    void LayerManager()
    {
        if (isOpen) return;

        if (displayImage.GetComponent<DisplayImage>().CurrentState == DisplayImage.State.zoom)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.layer = 2;
            }
        }
        else
        {
            foreach (Transform child in transform)
            {
                child.gameObject.layer = 0;
            }
        }
    }
     
}
