﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PickUpItem : MonoBehaviour, IInteractable
{

    public string DisplaySprite;

    public enum property { usable, displayable, dragable };

    public string DisplayImage;

    public string CombinationItem;

    public property itemProperty;

    private GameObject InventorySlots;
    private Pista[] InventoryPistas;
    private int papelcolor;

    public void Interact(DisplayImage currentDisplay)
    {
        ItemPickUp();
    }

    void Start()
    {
        papelcolor = 0;

    }

    public void ItemPickUp()
    {
        AudioSource sonido = GameObject.Find("Agarrar").GetComponent<AudioSource>();
        sonido.Play();

        InventorySlots = GameObject.Find("Slots");
        InventoryPistas = FindObjectsOfType<Pista>();
        Inventory inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
        
        GameObject inventoryPistasObject = GameObject.Find("Inventory Pistas");
        InventoryPistas inventoryPistas = null;
        if (inventoryPistasObject != null)
        {
            inventoryPistas = GameObject.Find("Inventory Pistas").GetComponent<InventoryPistas>();
        }
        if (DisplaySprite == "foto" && inventoryPistas != null)
        {
            inventoryPistas.items += 1;
            InventoryPistas[1].transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Inventory Items/" + DisplaySprite);
            InventoryPistas[1].GetComponent<Pista>().AssignPistaProperty((int)itemProperty, DisplayImage);
            
            Destroy(gameObject);
        }
        else if (DisplaySprite == "llave" && inventoryPistas != null && SceneManager.GetActiveScene().name == "nivel1")
        {
            inventoryPistas.items += 1;
            InventoryPistas[2].transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Inventory Items/" + DisplaySprite);
            InventoryPistas[2].GetComponent<Pista>().AssignPistaProperty((int)itemProperty, DisplayImage);
            Destroy(gameObject);
        }
        else if (DisplaySprite == "direcciones" && inventoryPistas != null)
        {
            inventoryPistas.items += 1;
            InventoryPistas[3].transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Inventory Items/" + DisplaySprite);
            InventoryPistas[3].GetComponent<Pista>().AssignPistaProperty((int)itemProperty, DisplayImage);
            Destroy(gameObject);
        }
        else if(DisplaySprite == "copa" && inventoryPistas != null)
        {
            inventoryPistas.items += 1;
            InventoryPistas[0].transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Inventory Items/" + DisplaySprite);
            InventoryPistas[0].GetComponent<Pista>().AssignPistaProperty((int)itemProperty, DisplayImage);
            Destroy(gameObject);

        }
        else
        {
            foreach (Transform slot in InventorySlots.transform)
            {
                if (slot.transform.GetChild(0).GetComponent<Image>().sprite.name == "empty_item")
                {



                    inventory.items += 1;

                    if (DisplaySprite.StartsWith("lego", StringComparison.InvariantCultureIgnoreCase))
                    {
                        //slot.transform.GetChild(0).gameObject.AddComponent<Lego>();
                        // GameObject clone = Instantiate(gameObject, slot);
                        // transform.SetParent(slot.transform);
                        // gameObject.AddComponent<Lego>();
                        gameObject.tag = "Untagged";
                        //transform.position = slot.GetChild(0).transform.position;
                        Destroy(GetComponent<PickUpItem>());
                        gameObject.SetActive(false);
                        slot.GetComponent<Slot>().item = gameObject;
                        slot.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Inventory Items/" + DisplaySprite);
                        slot.GetComponent<Slot>().AssignProperty((int)itemProperty, DisplayImage, CombinationItem);
                    }
                    else
                    {
                        slot.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Inventory Items/" + DisplaySprite);
                        
                       /* if( DisplayImage== "0"){
                            slot.transform.GetChild(0).GetComponent<Image>().color = Color.red;
                        }else if  (DisplayImage =="1"){
                            slot.transform.GetChild(0).GetComponent<Image>().color = Color.blue;
                            
                        }else if  (DisplayImage=="2"){
                            slot.transform.GetChild(0).GetComponent<Image>().color = Color.yellow;
                            
                        }else if  (DisplayImage =="3"){

                            slot.transform.GetChild(0).GetComponent<Image>().color = Color.green;
                        }*/

                        slot.transform.GetChild(0).GetComponent<Image>().color = GetComponent<SpriteRenderer>().color;
                        slot.GetComponent<Slot>().AssignProperty((int)itemProperty, DisplayImage, CombinationItem);
                        if(inventoryPistas.papelcolor == 0 && SceneManager.GetActiveScene().name == "nivel2" && gameObject.name != "key_grey"){
                            inventoryPistas.papelcolor++;
                            inventoryPistas.itemDisplayer.SetActive(true);
                            inventoryPistas.itemDisplayer.GetComponent<SpriteRenderer>().sprite =
                                Resources.Load<Sprite>("Inventory Items/mensajes8");
                        }
                        Destroy(gameObject);
                    }
                    if (DisplaySprite.StartsWith("papel", StringComparison.InvariantCultureIgnoreCase))
                    {
                        inventory.trozosCarta.Add(DisplaySprite);
                        if(inventory.trozosCarta.Count == 1){
                            inventoryPistas.itemDisplayer.SetActive(true);
                            inventoryPistas.itemDisplayer.GetComponent<SpriteRenderer>().sprite =
                                Resources.Load<Sprite>("Inventory Items/mensajes1");
                        }

                    }

                    var scrollbar = GameObject.Find("Scrollbar Horizontal");
                    if (inventory.items > 5)
                    {

                        scrollbar.GetComponent<Scrollbar>().value += 0.18f;
                    }

                    break;
                }
            }
        }

    }


}
