﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Puzzle : MonoBehaviour {

	public bool IsCompleted { get; private set; }
    private bool itemSpawn;

    public GameObject direcciones;
    public bool completado = false;
    private DisplayImage displayImage;
    GameObject inventory;

    void Start()
    {
        inventory = GameObject.Find("Inventory");
        itemSpawn = false;
        displayImage = GameObject.Find("displayImage").GetComponent<DisplayImage>();
    }

    void Update()
    {
        if (CompletePuzzle() && !itemSpawn)
        {
            AudioSource sonido = GameObject.Find("Acierto").GetComponent<AudioSource>();
            sonido.Play();
            completado = true;
            var claimItem = Instantiate(direcciones  );
            direcciones.SetActive(true);
            gameObject.SetActive(false);
            displayImage.CurrentState = DisplayImage.State.normal;
            itemSpawn = true;
            inventory.GetComponent<Inventory>().removerPapeles();
         
        }
        HideDisplay();
    }

    void HideDisplay()
    {
        if(Input.GetMouseButtonDown(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            displayImage.CurrentState = DisplayImage.State.normal;
            this.gameObject.SetActive(false);
        }

        if(displayImage.GetComponent<DisplayImage>().CurrentState == DisplayImage.State.normal)
        {
            this.gameObject.SetActive(false);
        }
    }

    public bool CompletePuzzle()
    {
        if (IsCompleted) return true;

        IsCompleted = true;

        var puzzlePieces = FindObjectsOfType<PuzzlePiece>();

        foreach(PuzzlePiece puzzlePiece in puzzlePieces)
        {
            if(!(int.Parse(puzzlePiece.gameObject.name.ToString().Substring(puzzlePiece.gameObject.name.Length - 1)) ==
                int.Parse(puzzlePiece.gameObject.GetComponent<Image>().sprite.name.ToString().Substring(puzzlePiece.gameObject.GetComponent<Image>().sprite.name.Length - 1))))
            {
                IsCompleted = false;
            }
        }
        
        return IsCompleted;
    }
}
