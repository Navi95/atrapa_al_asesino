﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{

    public GameObject currentSelectedSlot { get; set; }
    public GameObject previousSelectedSlot { get; set; }

    private GameObject slots;
    public GameObject itemDisplayer { get; private set; }
    public List<string> trozosCarta;
    public List<GameObject> listaPapeles;
    public DisplayImage currentDisplay { get; set; }
    public Puzzle puzzleReference;
    public int items = 0;

    void Start()
    {
        listaPapeles = new List<GameObject>();
        currentDisplay = GameObject.Find("displayImage").GetComponent<DisplayImage>();
        trozosCarta = new List<string>();
        InitializeInventory();
    }

    void Update()
    {

        SelectSlot();
        HideDisplay();
        verificarPapeles();

    }

    void InitializeInventory()
    {
        slots = GameObject.Find("Slots");
        itemDisplayer = GameObject.Find("ItemDisplayer");
        itemDisplayer.SetActive(false);
        foreach (Transform slot in slots.transform)
        {
            slot.transform.GetChild(0).GetComponent<Image>().sprite =
                Resources.Load<Sprite>("Inventory Items/empty_item");
            slot.GetComponent<Slot>().ItemProperty = Slot.property.empty;
        }
        currentSelectedSlot = GameObject.Find("slot");
        previousSelectedSlot = currentSelectedSlot;
    }

    void SelectSlot()
    {
        foreach (Transform slot in slots.transform)
        {
            if (slot.gameObject == currentSelectedSlot && slot.GetComponent<Slot>().ItemProperty == Slot.property.usable)
            {
                slot.GetComponent<Image>().color = new Color(.9f, .4f, .6f, 1);
            }
            else if (slot.gameObject == currentSelectedSlot && slot.GetComponent<Slot>().ItemProperty == Slot.property.displayable)
            {
                //slot.GetComponent<Slot>().DisplayItem();
            }
            else
            {
                slot.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            }
        }
    }

    void HideDisplay()
    {
        if (Input.GetMouseButtonDown(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            itemDisplayer.SetActive(false);
            if (currentSelectedSlot.GetComponent<Slot>().ItemProperty == Slot.property.displayable)
            {
                currentSelectedSlot = previousSelectedSlot;
                previousSelectedSlot = currentSelectedSlot;
            }
        }
    }

    void verificarPapeles()
    {
        if (trozosCarta.Count == 8 && !puzzleReference.completado)
        {
            puzzleReference.gameObject.SetActive(true);
            currentDisplay.CurrentState = DisplayImage.State.puzzle;
        }
    }


    public void removerPapeles()
    {
        foreach (Transform slot in slots.transform)
        {

            if(slot.transform.GetChild(0).GetComponent<Image>().sprite.name.StartsWith("papel", StringComparison.InvariantCultureIgnoreCase))
            {
                slot.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Inventory Items/empty_item");
                slot.GetComponent<Slot>().ItemProperty = Slot.property.empty;
            }
           
        }
        ajustarSlots();
        var scrollbar = GameObject.Find("Scrollbar Horizontal");
        scrollbar.GetComponent<Scrollbar>().value = 0; ;
    }

    void ajustarSlots()
    {
        int index = 0;
        Queue<int> slotVacios = new Queue<int>();
        foreach (Transform slot in slots.transform)
        {

            if (slot.transform.GetChild(0).GetComponent<Image>().sprite.name.StartsWith("empty_item", StringComparison.InvariantCultureIgnoreCase))
            {
                slotVacios.Enqueue(index);
                index += 1;
            }

        }
        foreach (Transform slot in slots.transform)
        {

            if (!slot.transform.GetChild(0).GetComponent<Image>().sprite.name.StartsWith("empty_item", StringComparison.InvariantCultureIgnoreCase))
            {
                if(slotVacios.Count > 0)
                {
                    int i = slotVacios.Dequeue();
                    slots.transform.GetChild(i).transform.GetChild(0).transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Inventory Items/" + slot.transform.GetChild(0).GetComponent<Image>().sprite.name);
                    slots.transform.GetChild(i).transform.GetComponent<Slot>().ItemProperty = slot.transform.GetComponent<Slot>().ItemProperty;
                    slots.transform.GetChild(i).transform.GetComponent<Slot>().item = slot.transform.GetComponent<Slot>().item;

                    //clear slot
                    slot.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Inventory Items/empty_item");
                    slot.GetComponent<Slot>().ItemProperty = Slot.property.empty;
                }
                
            }

        }

    }
}

