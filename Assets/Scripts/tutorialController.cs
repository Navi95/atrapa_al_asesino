﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class tutorialController : MonoBehaviour {

	int count;
	GameObject window1;
	GameObject window2;
	GameObject window3;
	GameObject window4;

	// Use this for initialization
	void Start () {
		count = 1;
		window1 = GameObject.Find("tutorial1");
		window2 = GameObject.Find("tutorial2");
		window3 = GameObject.Find("tutorial3");
		window4 = GameObject.Find("tutorial4");

		window2.SetActive(false);
		window3.SetActive(false);
		window4.SetActive(false);
		


	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetMouseButtonDown(0)){
			switch (count)
			{
				case 1:
					window1.SetActive(false);
					window2.SetActive(true);
					count++;
					break;

				case 2:
					window2.SetActive(false);
					window3.SetActive(true);
					count++;
					break;					
				case 3:
					window3.SetActive(false);
					window4.SetActive(true);
					count++;
					break;				
				case 4:
					SceneManager.LoadScene("nivel1");
					break;

				default:
					break;
			}			
		}
		
	}
}
