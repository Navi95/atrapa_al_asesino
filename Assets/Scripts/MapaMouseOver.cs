﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MapaMouseOver : MonoBehaviour {

	private string name;
	private Image Tivan;
	private Image Leila;

	// Use this for initialization
	void Start () {

		name = gameObject.name;
		Tivan = GameObject.Find("iconTivan").GetComponent<Image>();
		Leila = GameObject.Find("iconLeila").GetComponent<Image>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseEnter()
	{
		if(name == "addressTivan"){
			GetComponent<Image>().sprite = Resources.Load<Sprite>("Mapa/addressTivan2");
			Tivan.sprite = Resources.Load<Sprite>("Mapa/iconTivan");
		}else{
			GetComponent<Image>().sprite = Resources.Load<Sprite>("Mapa/addressLeila2");
			Leila.sprite = Resources.Load<Sprite>("Mapa/iconLeila");
		}

	}
	void OnMouseExit()
	{
		if(name == "addressTivan"){
			GetComponent<Image>().sprite = Resources.Load<Sprite>("Mapa/addressTivan");
			Tivan.sprite = Resources.Load<Sprite>("Sprites/Empty");

		}else{
			GetComponent<Image>().sprite = Resources.Load<Sprite>("Mapa/addressLeila");
			Leila.sprite = Resources.Load<Sprite>("Sprites/Empty");
		}
	}
}
