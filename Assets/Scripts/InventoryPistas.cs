﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class InventoryPistas : MonoBehaviour
{

    public GameObject currentSelectedPista { get; set; }
    public GameObject previousSelectedPista { get; set; }

    private GameObject pistas;
    public GameObject itemDisplayer { get;  set; }
    public GameObject ending;
   
    public int items = 0;
    public int papelcolor = 0;

    void Start()
    {
        InitializeInventory();
        ending = GameObject.Find("Ending");
        ending.SetActive(false);
    }

    void Update()
    {

        SelectPista();
        HideDisplay();
        verificarFin();

    }

    void InitializeInventory()
    {
        pistas = GameObject.Find("Pistas");
        itemDisplayer = GameObject.Find("PistaDisplayer");
        itemDisplayer.SetActive(false);

       foreach (Transform pista in pistas.transform){
            pista.transform.GetChild(0).GetComponent<Image>().sprite =
                Resources.Load<Sprite>("Inventory Items/empty_item");
            pista.GetComponent<Pista>().ItemProperty = Pista.pistaProperty.empty;
        }
        
        currentSelectedPista = GameObject.Find("pista");
        previousSelectedPista = currentSelectedPista;
    }

    void SelectPista()
    {
        foreach (Transform pista in pistas.transform)
        {
            if (pista.gameObject == currentSelectedPista && pista.GetComponent<Pista>().ItemProperty == Pista.pistaProperty.displayable)
            {
                //Pista.GetComponent<Pista>().DisplayItem();
            }
            else
            {
                pista.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            }
        }
    }

    void HideDisplay()
    {
        if (Input.GetMouseButtonDown(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            itemDisplayer.SetActive(false);
            if (currentSelectedPista.GetComponent<Pista>().ItemProperty == Pista.pistaProperty.displayable)
            {
                currentSelectedPista = previousSelectedPista;
                previousSelectedPista = currentSelectedPista;
            }
        }
    }

    void verificarFin() {
        if(items == 4)
        {
            ending.SetActive(true);
        }
    }   

}

