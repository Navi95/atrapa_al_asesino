﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Pista : MonoBehaviour, IPointerClickHandler
{
    private GameObject inventoryPistas;

    public enum pistaProperty {usable, displayable, dragable, empty };
    public pistaProperty ItemProperty { get; set; }

    private string displayImage;

    void Start()
    {
        inventoryPistas = GameObject.Find("Inventory Pistas");
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        inventoryPistas.GetComponent<InventoryPistas>().previousSelectedPista = inventoryPistas.GetComponent<InventoryPistas>().currentSelectedPista;
        inventoryPistas.GetComponent<InventoryPistas>().currentSelectedPista = this.gameObject;
        if (ItemProperty == Pista.pistaProperty.displayable) DisplayItem();
    }

    public void AssignPistaProperty(int orderNumber, string displayImage)
    {
        ItemProperty = (pistaProperty)orderNumber;
        this.displayImage = displayImage;
    }

    public void DisplayItem()
    {
        inventoryPistas.GetComponent<InventoryPistas>().itemDisplayer.SetActive(true);
        inventoryPistas.GetComponent<InventoryPistas>().itemDisplayer.GetComponent<SpriteRenderer>().sprite =
            Resources.Load<Sprite>("Inventory Items/" + displayImage);
    }



    public void ClearSlot()
    {
        inventoryPistas.GetComponent<InventoryPistas>().items -= 1;
        var scrollbar = GameObject.Find("Scrollbar Horizontal");
        scrollbar.GetComponent<Scrollbar>().value -= 0.18f;

        ItemProperty = Pista.pistaProperty.empty;
        displayImage = "";
        transform.GetChild(0).GetComponent<Image>().sprite =
            Resources.Load<Sprite>("Inventory Items/empty_item");
    }
}
