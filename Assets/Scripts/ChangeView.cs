﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeView : MonoBehaviour, IInteractable {

    public string SpriteName;
    public string tipo;


    void Awake()
    {
        
    }

    public void Interact(DisplayImage currentDisplay)
    {

        currentDisplay.escenaAnterior = currentDisplay.escenaActual;
        currentDisplay.pilaEscenas.Push(currentDisplay.escenaActual);
        currentDisplay.escenaActual = SpriteName;


        if(tipo == "puerta"){
            AudioSource sonido  = GameObject.Find("PuertaAbierta").GetComponent<AudioSource>();
            sonido.Play();
        }


    }


  

}
