﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentButton : MonoBehaviour {

    public string siguienteEscena ;

    public enum Button  {
    	left, right, down
    };

    public Button botonActivo;
    private DisplayImage currentDisplay;
    
    void Start()
    {
    	currentDisplay = GameObject.Find("displayImage").GetComponent<DisplayImage>();
        
    }

    void Update()
    {
        if ( botonActivo == Button.left ){
	        currentDisplay.CurrentActiveButton = DisplayImage.ButtonNavigation.left ;
        }
        if( botonActivo == Button.right ){
	        currentDisplay.CurrentActiveButton = DisplayImage.ButtonNavigation.right ;
            
        }
        if( botonActivo == Button.down ){
	        currentDisplay.CurrentActiveButton = DisplayImage.ButtonNavigation.down ;
        }
        currentDisplay.siguienteEscena = siguienteEscena;

    }

    

}
