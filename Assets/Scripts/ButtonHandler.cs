﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonHandler : MonoBehaviour {

    private DisplayImage currentDisplay;


    private float initialCameraSize;
    private Vector3 initialCameraPosition;

    private ZoomInObject[] zoomInObjects;
 

    private void Start()
    {
            currentDisplay = GameObject.Find("displayImage").GetComponent<DisplayImage>();
            if(currentDisplay != null){
                currentDisplay.CurrentState = DisplayImage.State.normal;  
            }
        
       
    }

    void Awake()
    {
        currentDisplay = GameObject.Find("displayImage").GetComponent<DisplayImage>();
        initialCameraSize = Camera.main.orthographicSize;
        initialCameraPosition = Camera.main.transform.position;

        zoomInObjects = FindObjectsOfType<ZoomInObject>();
        

       
    }

    public void OnClickLeftRightArrow()
    {
        currentDisplay.escenaAnterior = currentDisplay.escenaActual;
        currentDisplay.escenaActual = currentDisplay.siguienteEscena;

    }

    public void OnClickDownArrow()
    {

        currentDisplay.escenaActual = currentDisplay.pilaEscenas.Pop().ToString();

    }

    public void OnClickZoomButton()
    {
        //if(ZoomTimes ==0 ){
        //        print("mostrar");
        //       Notificacion Notificacion =  GameObject.Find("Notificacion").GetComponent<Notificacion>();
        //       Notificacion.show = true;
        //       Notificacion.text = "Presione la tecla Esc para cerrar la lupa";
        //    }
        //    ZoomTimes  = ZoomTimes +1;
        if(currentDisplay.CurrentState == DisplayImage.State.zoom){
            
            currentDisplay.CurrentState  = DisplayImage.State.normal;
            
        }else if(currentDisplay.CurrentState == DisplayImage.State.normal){
            currentDisplay.CurrentState  = DisplayImage.State.zoom;
        }
    }


    public void OnClickReturn()
    {
        if (currentDisplay.CurrentState == DisplayImage.State.zoom) {
            currentDisplay.CurrentState = DisplayImage.State.normal;

            foreach (var zoomInObject in zoomInObjects)
            {
                zoomInObject.gameObject.layer = 0;
            }

            Camera.main.orthographicSize = initialCameraSize;
            Camera.main.transform.position = initialCameraPosition;
        }
      
    }

    public void OnClickPlay()
    {

        SceneManager.LoadScene("intro");
    }

    public void OnClickStartTutorial(){
        SceneManager.LoadScene("introTutorial");
    }

    public void OnClickStartGame()
    {
        AudioSource sonido  = GameObject.Find("PuertaAbierta").GetComponent<AudioSource>();
        sonido.Play();
        SceneManager.LoadScene("introTutorial");
    }
    
    public void OnClickSound(Button button)
    {
        AudioSource music  = GameObject.Find("BackgroundMusic").GetComponent<AudioSource>();
        if(music.isPlaying){
            music.Stop();
            Settings.sonido = false;
            button.image.sprite= Resources.Load<Sprite>("Sprites/off");

        }else{
            music.Play();
            Settings.sonido = true;
            button.image.sprite= Resources.Load<Sprite>("Sprites/on");
            
        }
    }

    public void OnClickExit()
    {
        Application.Quit();
    }

    public void OnClickResume(GameObject pausemenu)
    {
        pausemenu.SetActive(false);
    }

    public void OnClickQuit()
    {
         SceneManager.LoadScene("menu");
    }

    public void OnClickStartGame(GameObject launcherScreenObject)
    {
 
        Destroy(launcherScreenObject);
    }

    public void OnClickInitialDoor(GameObject InitialScreenObject)
    {
        currentDisplay.CurrentState = DisplayImage.State.normal;

        Destroy(InitialScreenObject);
    }
}
