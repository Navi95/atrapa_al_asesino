﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
public class Scale : MonoBehaviour {

    public GameObject[] ScaleBoxes;
    public GameObject[] leftBoxes;
    public GameObject[] rightBoxes;
    public int[] Weight = new int[5];
    public GameObject scale, scaleSolved, cajon;
    public Animator animator;

    public GameObject ScaleDisplayer;

    private DisplayImage displayImage;
    private Block[] blocks;

    public bool isSolved { get; private set; }

    void Awake()
    {
        isSolved = false;
        displayImage =GameObject.Find("displayImage").GetComponent<DisplayImage>();
        blocks = FindObjectsOfType<Block>();
    }

    void Update()
    {
        if (!isSolved)
        {
            Display();
        }

        if (VerifySolution() && !isSolved)
        {
            isSolved = true;
            // cajon.GetComponent<SpriteRenderer>().sortingOrder = 0;
            //ScaleDisplayer.GetComponent<ChangeView>().SpriteName = "scale solved";
            scaleSolved.SetActive(true);
            scale.SetActive(false);
            AudioSource sonido = GameObject.Find("Chapa").GetComponent<AudioSource>();
            sonido.Play();
            animator.SetBool("abierto", true);
            //displayImage.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/scale solved");
           // displayImage.escenaActual = "scale solved";
           // GameObject scalesolved = GameObject.Find("scale solved");
           // GameObject scale = GameObject.Find("scale");
           // scale.transform.parent = scaleSolved.transform; 
        }
    }

    void Display()
    {
        if(displayImage.GetComponent<SpriteRenderer>().sprite.name.StartsWith("scale", StringComparison.InvariantCultureIgnoreCase) )
        {
            for(int i = 0; i < blocks.Length; i++)
            {
                blocks[i].gameObject.SetActive(true);
            }
            cajon.SetActive(true);
            
                
        }
        else
        {
            for (int i = 0; i < blocks.Length; i++)
            {
                blocks[i].gameObject.SetActive(false);
            }
        }
    }

    bool VerifySolution()
    {
        int leftSum = getLeftSum();
        int rightSum = getRightSum();
               
        
        if(rightSum > 0 && leftSum >0 && rightSum == leftSum ){
            
            return true;
        }
        return false;

       

    }

    public int getLeftSum(){
        int suma =0;
        for(int i = 0; i < blocks.Length; i++)
        {
            if(blocks[i].indexOfBox == 0 ||  blocks[i].indexOfBox == 1 || blocks[i].indexOfBox == 2 ){
                suma = suma + blocks[i].weight;
            }
        }
        return suma;
    }

    public int getRightSum(){
        int suma =0;
        for(int i = 0; i < blocks.Length; i++)
        {
            if(blocks[i].indexOfBox == 3 ||  blocks[i].indexOfBox == 4  ){
                suma = suma + blocks[i].weight;
            }
        }
        return suma;
    }



    
}
