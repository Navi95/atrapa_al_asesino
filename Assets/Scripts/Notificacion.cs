﻿using UnityEngine;
 using System.Collections;
 using System;
 
public class Notificacion: MonoBehaviour {
 
     /*
     * Start Copying Here
     */
     public bool show =false;
     public string text ;
     private bool showText = false;
     private float currentTime = 0.0f, executedTime = 0.0f, timeToWait = 5.0f;
     
     void OnMouseDown()
     {
         executedTime = Time.time;
     }
     
     void Update()
     {
         currentTime = Time.time;
         if(show)
             showText = true;
         else
             showText = false;
         
         if(executedTime != 0.0f)
         {
         	
             if(currentTime - executedTime > timeToWait)
             {
             	print("time out");
                 executedTime = 0.0f;
                 show = false;
             }
         }
     }
     
     void OnGUI()
     {
         if(showText)
             GUI.Label(new Rect(0, 0, 100, 100),  text);
     }
     
     /*
     * Stop Copying
     */
 }