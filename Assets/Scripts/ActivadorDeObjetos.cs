﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ActivadorDeObjetos : MonoBehaviour {

    private DisplayImage currentDisplay;

    public GameObject[] ObjectsToMange;
    public GameObject[] UIRenderObjects;
    private int scaleCount = 0;
    private int libreroCount = 0;
    public GameObject scale;
    public GameObject cajita;

    void Start()
    {
        currentDisplay = GameObject.Find("displayImage").GetComponent<DisplayImage>();
    }

    void Update()
    {
        MangeObjects();
    }

    void MangeObjects()
    {
        for (int i = 0; i < ObjectsToMange.Length; i++)
        {
            if(currentDisplay.CurrentState ==  DisplayImage.State.puzzle){
                ObjectsToMange[i].SetActive(false);
             }else{
                if(ObjectsToMange[i].name == currentDisplay.GetComponent<SpriteRenderer>().sprite.name)
                {
                    ObjectsToMange[i].SetActive(true);
                    if (currentDisplay.GetComponent<SpriteRenderer>().sprite.name == "scale" )
                    {
                        scale.SetActive(true);
                    }
                    else
                    {
                        scale.SetActive(false);
                    }
                    if(cajita != null){
                        if (currentDisplay.GetComponent<SpriteRenderer>().sprite.name == "zoom escritorio")
                            cajita.SetActive(true);
                        else
                            cajita.SetActive(false);
                    }
                }
                else
                {
                        ObjectsToMange[i].SetActive(false);
                    
                }
             }
            
        }
    }

    void RenderUI()
    {
        for(int i = 0; i < UIRenderObjects.Length; i++)
        {
            UIRenderObjects[i].SetActive(false);
        }
    }
}
