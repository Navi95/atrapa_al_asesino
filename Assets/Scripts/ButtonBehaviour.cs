﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBehaviour : MonoBehaviour {

	public enum ButtonId
    {
        roomChangeButton, returnButton , left, right, down , zoomOut, zoom
    }

    public ButtonId ThisButtonId;

    private DisplayImage currentDisplay;

    void Start()
    {
        currentDisplay = GameObject.Find("displayImage").GetComponent<DisplayImage>();
    }

    void Update()
    {
        if(currentDisplay.CurrentActiveButton == DisplayImage.ButtonNavigation.left  && ThisButtonId == ButtonId.left ){
            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g,
                                                    GetComponent<Image>().color.b, 1);
            GetComponent<Button>().enabled = true;

        }else if(currentDisplay.CurrentActiveButton == DisplayImage.ButtonNavigation.right  && ThisButtonId == ButtonId.right ){
            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g,
                                                    GetComponent<Image>().color.b, 1);
            GetComponent<Button>().enabled = true;

        }else if(currentDisplay.CurrentActiveButton == DisplayImage.ButtonNavigation.down  && ThisButtonId == ButtonId.down ){
            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g,
                                                    GetComponent<Image>().color.b, 1);
            GetComponent<Button>().enabled = true;
        }
        
       // else if (currentDisplay.CurrentState == DisplayImage.State.zoom && ThisButtonId == ButtonId.zoomOut){
       //     GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g,
       //                                             GetComponent<Image>().color.b, 1);
       //     GetComponent<Button>().enabled = true;
       // }
        else {
            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, 0);
            GetComponent<Button>().enabled = false;
            this.transform.SetSiblingIndex(0);
        }

       
    }

   

}
