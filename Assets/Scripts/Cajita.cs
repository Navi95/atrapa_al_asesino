﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cajita : MonoBehaviour
{
    


    private DisplayImage displayImage;
    public Lego[] legos;
    public LegoBox[] legoBoxes;
    public Animator animator;
    public GameObject objetoBloqueado;
    public bool isSolved { get; private set; }

    void Awake()
    {
        isSolved = false;
        objetoBloqueado.SetActive(true);
        displayImage = GameObject.Find("displayImage").GetComponent<DisplayImage>();
        
        legoBoxes = FindObjectsOfType<LegoBox>();
        objetoBloqueado.SetActive(false);
    }

    void Update()
    {
       

        if (VerifySolution() && !isSolved)
        {
            foreach (Lego lego in legos)
            {
                lego.gameObject.SetActive(false);
            }
            animator.SetBool("abierta", true);

            isSolved = true;
        }
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("CajitaAbierta"))
            {
                if(objetoBloqueado != null)
                objetoBloqueado.SetActive(true);

            }


     
       
    }

   

    bool VerifySolution()
    {
        if (legos.Length > 0)
        {
            foreach (Lego lego in legos)
            {
                if (lego.forma != lego.formaContenedora)
                {
                    return false;
                }
            }
            return true;
        }
        else
            return false;



    }

  


}
