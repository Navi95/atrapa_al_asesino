﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;


public class EndingN1 : MonoBehaviour {

	int count = 0;
	private Image img;

	// Use this for initialization
	void Start () {
		img = GetComponent<Image>();

	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetMouseButtonDown(0)){
			print(count);
			switch (count)
			{
				case 0 :
					img.sprite = Resources.Load<Sprite>("Ending/Sospechosos");
					count++;
					break;
				case 1:
					img.sprite = Resources.Load<Sprite>("Ending/Dialogo5");
					count++;
					break;
				case 2:
					SceneManager.LoadScene("mapa");
					break;				
				default:
					break;
			}
		}
		
	}
}
