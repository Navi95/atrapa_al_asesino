﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LibreroPuzzle : MonoBehaviour {

	public List<char> solution;
	public char[] name;
	public bool solved = false;
	public GameObject cofre;

	// Use this for initialization
	void Start () {

		Aleatoriedad random = GameObject.Find("Random").GetComponent<Aleatoriedad>();
		name = random.letras;
		cofre = GameObject.Find("cofreLibrero");	
		cofre.SetActive(false);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void checkSolution(){

		int count = 0;
		print(this.solution.Count);
		if (this.solution.Count == 5)
		{
			print(solution[0]+""+solution[1]+""+solution[2]+""+solution[3]+""+solution[4]);
			for (int i = 0; i < solution.Count; i++)
			{
				if(name[i] == solution[i]){
					count++;
				}
			}

			if(count == 5){
				//Abrir el cofre
				cofre.SetActive(true);
				Debug.Log("Solucionado");
				AudioSource audio = GameObject.Find("Chapa").GetComponent<AudioSource>();
				audio.Play();

					
			}else{
				solved = false;
				Book[] books = GameObject.FindObjectsOfType<Book>();
				solution.Clear();
				for (int j = 0; j < books.Length; j++)
				{
					books[j].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/empty_item2");
					books[j].state = false;
				}
			}
		}
	}
}
