﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class DisplayImage : MonoBehaviour {

    public string escenaActual  ;
    public string siguienteEscena ;
    public string escenaAnterior ;
    public Stack  pilaEscenas;
    public enum State
    {
        normal, ChangedView, idle , zoom , puzzle
    };

    public enum ButtonNavigation
    {
        left , right, down
    };

    public Image img;




    public State CurrentState { get; set; }

    public ButtonNavigation CurrentActiveButton { get; set; }
    public int CurrentWall
    {
        get { return currentWall; }
        set
        {
            if (value == 3)
                currentWall = 1;

            else if (value == 0)
                currentWall = 2;
            else
                currentWall = value;
        }
    }
    
    public int currentWall;
    public int previousWall;

    private int scaleCount = 0;
    private int libreroCount = 0;
    private int cofreCount = 0;
    private int puertaCount = 0;
    public bool mostrar = false;
    public bool llave = false;
    private int zoomCount = 0;
    private int cajaFCount = 0;
    private int cofre2Count = 0;

    public GameObject ending;


    void Start()
    {
        pilaEscenas = new Stack();
        CurrentState = State.idle;
        previousWall = 0;
        currentWall = 1;
        if(SceneManager.GetActiveScene().name != "menu")
        {
            img = GameObject.Find("mensajeRetroalimentacion").GetComponent<Image>();
        }
        if(ending != null){
            ending.SetActive(false);        
        }

    }

    void Update()
    {
        
   
        verificarSonido(); 
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/" + escenaActual.ToString());

        if(img != null)
        {
             if (Input.GetMouseButtonDown(0))
            {
                img.sprite = Resources.Load<Sprite>("Sprites/Empty");
            }
            
            if (mostrar && this.puertaCount == 0)
            {
                img.sprite = Resources.Load<Sprite>("Sprites/mensajeLlave");
                puertaCount++;
                return;
            }
            if (mostrar && llave)
            {
                img.sprite = Resources.Load<Sprite>("Sprites/mensajeLlaveNo");
                llave = false;
                return;
            }
            if (escenaActual == "scale" && this.scaleCount == 0)
            {
                img.sprite = Resources.Load<Sprite>("Sprites/mensajeBalanza");
                scaleCount++;
            }
            if (escenaActual == "librero wall1" && this.libreroCount == 0)
            {
                img.sprite = Resources.Load<Sprite>("Sprites/mensajeLibrero");
                libreroCount++;
            }
            if (escenaActual == "cofre" && this.cofreCount == 0)
            {
                img.sprite = Resources.Load<Sprite>("Sprites/mensajeCofre");
                cofreCount++;
            }
            if (escenaActual == "zoom escritorio" && this.zoomCount == 0)
            {
                img.sprite = Resources.Load<Sprite>("Sprites/mensajeCajita");
                zoomCount++;
            }
            if (escenaActual == "caja fuerte" && this.cajaFCount == 0)
            {
                img.sprite = Resources.Load<Sprite>("Sprites/mensajeCajafuerte");
                cajaFCount++;
            }
            if (escenaActual == "cofre2" && this.cofre2Count == 0)
            {
                img.sprite = Resources.Load<Sprite>("Sprites/mensajeCofre");
                cofre2Count++;
            }
            if(escenaActual == "cocina"){
                ending.SetActive(true);
            }

           
        }
        

        
    }

    void verificarSonido(){
    	AudioSource music = GameObject.Find("BackgroundMusic").GetComponent<AudioSource>();
    	if(!Settings.sonido){
    		if(music.isPlaying){
    			music.Stop();
    		}
    	}else{
    		if(!music.isPlaying){
    			music.Play();
    		}
    		
    	}
    	
       
    }

}
