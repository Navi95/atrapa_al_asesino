﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {
	private bool rotating = true;
	public Scale scale;

	// Use this for initialization
	void Start () {
		rotateLeft();
	}
	
	// Update is called once per frame
	void Update () {
		 VerifyRotation();

	 }

	 public void VerifyRotation(){
	 	rotating =true;
        if(scale.getRightSum() >scale.getLeftSum()){
            rotateRight();	
        }else if(  scale.getRightSum() < scale.getLeftSum()){
            rotateLeft();
        }else if( scale.getRightSum() > 0 && scale.getLeftSum() >0 && scale.getRightSum() == scale.getLeftSum()){
            balance();
        }
    }

	 public void rotateLeft(){
	 	float z = 0;
       while(z<10){
    		z += Time.deltaTime * 10;
	        transform.rotation = Quaternion.Euler(0,0,10);
    	}
        
     }

     public void rotateRight(){
     	float z = 0;
        while(z<10){
    		z += Time.deltaTime * 10;
	        transform.rotation = Quaternion.Euler(0,0,-10);
    	}
        
     }



     public void balance(){
        transform.rotation = Quaternion.Euler(0,0,0);
        
     }

     


	 
}
