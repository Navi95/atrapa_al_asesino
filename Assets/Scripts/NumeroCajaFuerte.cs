﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumeroCajaFuerte : MonoBehaviour , IInteractable
{
    public string numero;

    CajaFuerte cajaFuerte;
    public void Interact(DisplayImage currentDisplay)
    {
        AudioSource sonido = GameObject.Find("Tecla").GetComponent<AudioSource>();
        sonido.Play();
        cajaFuerte.numerosPulsados.Enqueue(this);
    }

    // Use this for initialization
    void Start () {
		cajaFuerte = FindObjectOfType<CajaFuerte>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
